# Magnetic Field in a Toroidal Solenoid  POV-Ray and Associated files

These are the POV-Ray files used to create
[Magnetic Field in a Toroidal Solenoid](https://youtu.be/B40OWDZ-Tf4)

There are two parts to the numerical calculation of the field lines:

- approximating the magnetic field of the loops by summing the contributions of loop segments
- integrating the fieldlines along the direction of the field

Because all of this is accomplished within POV-Ray, the individual fames take quite a while to calculate and render.

1. Edit toroid_from_loops.pov to uncomment/comment sections for part1 or part2
2. Run the appropriate .ini file from POV-Ray in the image resolution and format of your choice
   - be patient, this will take a great deal of time 
3. Re-edit toroid_from_loops.pov to uncomment/comment sections for the other part
4. Run the other .ini file for POV-Ray

At this point you will have 1800 image files ready to be combined inton a 30 fps animation.  I usually use ffmpeg and my command (run from the directory containing the image files) would be something like:
```
ffmpeg -framerate 30 -i toroid_from_loops%04d.png  -codec:v libx264  -crf 21 -bf 2 -flags +cgop -pix_fmt yuv420p -movflags faststart toroid_from_loops.mp4
```
