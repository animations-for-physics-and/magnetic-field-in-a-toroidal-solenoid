// PoVRay 3.7 Scene File " ... .pov"
// author:  ...
// date:    ...
//--------------------------------------------------------------------------
#version 3.7;
global_settings{ assumed_gamma 1.0 }
#default{ finish{ ambient 0.1 diffuse 0.9 }} 
//--------------------------------------------------------------------------
#include "colors.inc"
#include "textures.inc"
#include "glass.inc"
#include "metals.inc"
#include "golds.inc"
#include "stones.inc"
#include "woods.inc"
#include "shapes.inc"
#include "shapes2.inc"
#include "functions.inc"
#include "math.inc"
#include "transforms.inc"

#include "finish.inc"

//--------------------------------------------------------------------------
#declare trns = function(tt) { (1-cos(pi*max(min(tt,1),0)))/2 }
#declare lclock=clock+0;
//part1 frames 1-1050
/*
#declare T1=trns((lclock-2)/4);//pan camera from right behind to up and right
#declare T2=trns((lclock-8)/12);//spread loops
#declare T3=trns((lclock-25)/5);//pan camera to overhead
//wait 5
*/
//part2 frames 1051-1800
#declare T1=trns((lclock-0)/4);//pan camera from right behind to up and right
#declare T2=trns((lclock-2)/15);//spread loops
#declare T3=trns((lclock-17)/3);//pan camera to overhead
//wait 5

#declare NLoops=29;//odd # to avoid double spacing atlast ring
#declare Ncharges=8;
#declare loop_r=.05;
#declare loop_R=1;
#declare solenoid_r=loop_R-loop_r-.001;//shave a little extra off to aoid intersecting surfaces
#declare solenoid_R=5*solenoid_r;
#declare Loop1=torus { loop_R,loop_r   
    texture {
    pigment {
        radial 
        color_map {
            [0.0 color rgbt <.5,.5,.5,.95>]
            [0.3 color rgbt <.5,.5,.5,.95>]
            [0.6 color rgbt <.5,.25,.25,.5>]
            [0.65 color rgbt <1,.0,.0,0>]
            [0.7 color rgbt <1,.0,.0,0>]
            [.71 color rgbt <.5,.5,.5,.95>]
        [.99 color rgbt <.5,.5,.5,.95>]
        }
        frequency 8 rotate 3*y*360*lclock/Ncharges
    }
        finish{Luminous}  
    }             
 rotate -z*90        no_shadow  no_reflection
} // end of loop1  -------------------------------              


torus { solenoid_R,solenoid_r      pigment {color rgbt <1,1,1,.95>} finish{Glossy}}
#declare dQ=360/NLoops*T2;
//order 0 loop
object{Loop1   translate z*solenoid_R} 
//additional loops
#declare loopcount=1;
#while(loopcount<NLoops/2)
    object{Loop1 rotate x*360/(Ncharges*NLoops)*loopcount translate z*solenoid_R rotate y*dQ*loopcount} 
    object{Loop1 rotate -x*360/(Ncharges*NLoops)*loopcount translate z*solenoid_R rotate -y*dQ*loopcount} 

    #declare loopcount=loopcount+1;
#end    

#macro BL1(rfpt)
    #declare Bans=<0,0,0>;
    #declare nloopseg=12;
    #declare j=-(NLoops-1)/2;
    #while (j<=(NLoops-1)/2)
        
        #declare i=0;
        #declare r0i=y*loop_R;
        #declare ri=r0i+z*solenoid_R;
        #declare ri=vrotate(ri, j*dQ*y);
        #while(i<nloopseg)
            #declare r0ip1=vrotate(y*loop_R,(i+1)/nloopseg*360*x);
            #declare rip1=r0ip1+z*solenoid_R;
            #declare rip1=vrotate(rip1, j*dQ*y);
    
            #declare rrel=rfpt-.5*(ri+rip1);
            #declare Bans=Bans+vcross(rip1-ri,rrel)/pow(vlength(rrel),3);
            //calc B due to ri rip1 segment here
            //test markers for intervals
            //sphere{ri,.05 pigment{Blue}finish{Luminous} no_reflection no_shadow} 
            //sphere{(ri+rip1)/2,.05 pigment{Orange}finish{Luminous}} 
        
            #declare ri=rip1;
            #declare i=i+1;
        #end
    
        #declare j=j+1;
    #end        
 
#end

#declare ds=.05;
#declare fieldlinestartoffsetfactor=.5;
#declare Smax=80;
#declare fieldlineR=.02;

#declare RHS_Lines=union{
    #declare S=0;
        #declare fp=z*solenoid_R;
    #while((S<Smax)&(fp.x>=0))
        BL1(fp)
        #declare nfp=fp+ds*Bans/vlength(Bans); 
        union{
            cylinder{fp,nfp,fieldlineR  }
            sphere{nfp,fieldlineR }
            pigment{Green} finish{Luminous} no_shadow no_reflection
        }
        #declare fp=nfp;
        #declare S=S+ds;
    #end    

    #declare S=0;
        #declare fp=z*solenoid_R+fieldlinestartoffsetfactor*loop_R*z;
    #while((S<Smax)&(fp.x>=0))
        BL1(fp)
        #declare nfp=fp+ds*Bans/vlength(Bans); 
        union{
            cylinder{fp,nfp,fieldlineR }
            sphere{nfp,fieldlineR}
            pigment{Green} finish{Luminous} no_shadow no_reflection
        }
        #declare fp=nfp;
        #declare S=S+ds;
    #end    
    #declare S=0;
        #declare fp=z*solenoid_R-fieldlinestartoffsetfactor*loop_R*z;
    #while((S<Smax)&(fp.x>=0))
        BL1(fp)
        #declare nfp=fp+ds*Bans/vlength(Bans); 
        union{
            cylinder{fp,nfp,fieldlineR  }
            sphere{nfp,fieldlineR }
            pigment{Green} finish{Luminous} no_shadow no_reflection
        }
        #declare fp=nfp;
        #declare S=S+ds;
    #end    


    #declare S=0;      //use vertical symmetry!
        #declare fp=z*solenoid_R+fieldlinestartoffsetfactor*loop_R*y;
    #while((S<Smax)&(fp.x>=0))
        BL1(fp)
        #declare nfp=fp+ds*Bans/vlength(Bans); 
        union{
            cylinder{fp,nfp,fieldlineR  }
            sphere{nfp,fieldlineR }
            cylinder{fp,nfp,fieldlineR scale <1,-1,1> }    //mirror
            sphere{nfp,fieldlineR scale <1,-1,1>}          //mirror
            pigment{Green} finish{Luminous} no_shadow no_reflection
        }
        #declare fp=nfp;
        #declare S=S+ds;
    #end    
}

object{ RHS_Lines}
object{ RHS_Lines scale <-1,1,1> }
/**/



camera {
  location  vrotate(vrotate(z*(25-10*T3),-x*(T1*40+T3*49.5)),T1*15*y)
  look_at   <0.0, 0.0,  0>
  right     -x*image_width/image_height
}
// create a regular point light source
light_source {
  0*x                  // light's position (translated below)
  color rgb <1,1,1>    // light's color
  translate <-20, 40, -20>*900
}
